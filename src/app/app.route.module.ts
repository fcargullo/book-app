import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { BookSearchComponent } from './book-search/book-search.component';
import { BookAddComponent } from './book-add/book-add.component';

import { BookSearchResolver } from './resolvers/book-search.resolver';

const appRoutes: Routes = [
  {
    path: 'book-search',
    component: BookSearchComponent,
    resolve: { message: BookSearchResolver }
  },
  { path: 'book-add', component: BookAddComponent },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'book-search'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      enableTracing: false,
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
  providers: [BookSearchResolver]
})
export class AppRouteModule {}
