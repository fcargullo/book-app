import { Action } from '@ngrx/store';
import { Book } from '../models/book.model';

export const BOOK_REMOVE = '[BOOK] Remove';
export const BOOK_ADD    = '[BOOK] Add';

export class BookRemove implements Action {
    readonly type = BOOK_REMOVE;

    constructor(public payload: number) {}
}

export class BookAdd implements Action {
    readonly type = BOOK_ADD;

    constructor(public payload: Book) {}
}

export type Actions = BookRemove | BookAdd;
