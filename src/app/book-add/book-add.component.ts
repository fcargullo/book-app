import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import * as bookActions from '../actions/book.actions';
import { AppState } from '../app.state';
import { Book } from 'app/models/book.model';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.scss']
})
export class BookAddComponent implements OnInit {

  formData: Book;
  maxDate = new Date();

  constructor(
    private router: Router,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.defineFormData();
  }

  defineFormData() {
    this.formData = {
      isbn: undefined,
      title: undefined,
      subtitle: undefined,
      author: undefined,
      published: undefined,
      publisher: undefined,
      pages: undefined,
      description: undefined,
      website: undefined
    };
  }

  async submitForm() {
    this.store.dispatch(new bookActions.BookAdd(this.formData));
    await this.store.select('books').subscribe(data => {
      localStorage.setItem('books', JSON.stringify(data));
      alert('New entry has been successfully created.');
      this.router.navigate(['book-search']);
    });
  }
}
