import { Book } from '../models/book.model';
import * as BookActions from '../actions/book.actions';

export function reducer(state: Book[] = [], action: BookActions.Actions) {

    switch (action.type) {
        case BookActions.BOOK_REMOVE:
            state.splice(action.payload, 1);
            return state;

        case BookActions.BOOK_ADD:
            return [...state, action.payload];

        default:
            return state;
    }
}