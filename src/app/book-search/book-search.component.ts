import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
// import { Observable } from 'rxjs/Observable';
// import { Store } from '@ngrx/store';
import { Book } from './../models/book.model';
import { AppState } from './../app.state';
// import { BookService } from '../services/book.service';
import * as bookActions from '../actions/book.actions';

@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.scss']
})
export class BookSearchComponent implements OnInit {
  data: Book[];
  dataCopy: Book[];
  viewId: number;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.route.snapshot.data.message.subscribe((data) => {
      this.data = data;
      this.dataCopy = data;
    });
  }

  searchTitle(title) {
    const result = this.dataCopy.filter(x => x.title.toLowerCase().includes(title.toLowerCase()));
    this.data = result;
  }

  viewDetails(index) {
    this.viewId = index;
  }

  back() {
    this.viewId = undefined;
    this.data = this.dataCopy;
  }

  async deleteBook() {
    if (confirm('Are you sure you want to delete this book?')) {
      this.store.dispatch(new bookActions.BookRemove(this.viewId));
      await this.store.select('books').subscribe(data => {
        this.dataCopy = data;
        localStorage.setItem('books', JSON.stringify(data));
      });

      this.back();
    }
  }
}
