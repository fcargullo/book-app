import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Store } from '@ngrx/store';

import * as bookActions from '../actions/book.actions';
import { BookService } from '../services/book.service';

import { Book } from '../models/book.model';
import { AppState } from '../app.state';

@Injectable()
export class BookSearchResolver implements Resolve<Observable<Book[]>> {
  storeData: Book[];
  apiData: Book[];

  constructor(
    private bookService: BookService,
    private store: Store<AppState>
  ) {}

  resolve() {
    this.initBookData();
    return Observable.of(this.getBookData());
  }

  initBookData(): void {

    const localData = JSON.parse(localStorage.getItem('books'));

    if (!localData || localData.length === 0) {
      this.bookService.getBooks().subscribe((data) => {
        localStorage.setItem('books', JSON.stringify(data));
        this.syncState(data);
      }, (err) => {
        alert('Ooops... Something went wrong!!! Please contact admin.');
      });
    } else {
        this.syncState(localData);
    }
  }

  getBookData() {
    return this.store.select('books');
  }

  syncState(data) {
    this.getBookData().subscribe(response => {
      if (!response || response.length === 0) {
        for (const value of data) {
            this.store.dispatch(new bookActions.BookAdd(value));
        }
      }
    });
  }
}
