# BookApp

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

## Dependency Installation
In root folder, run `npm install`.

## API JSON-SERVER 
Install JSON-SERVER by running this command `npm install -g json-server`.
In app root folder, run `json-server --watch ./src/assets/db.json`.

## Development server
Run `ng serve` for a dev server. 
Navigate to `http://localhost:4200/`. 
The app will automatically reload if you change any of the source files.

